CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers


INTRODUCTION
------------

The Breadcrumb Field module provides configurable breadcrumbs that improve on
core breadcrumbs by including the breadcrumb field with the possibility to
override current page crumbs.
This module is currently available for Drupal 10.x.

Breadcrumb Field can be used with nodes and taxonomy term entities.
You can add the breadcrumb field type to these entities.
Also, Breadcrumb Field uses the current URL (path alias) and
the current page's title
to automatically extract the breadcrumb's segments and their respective links.

REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

Install the Breadcrumb Field module as you would normally install a contributed
Drupal module. Visit https://www.drupal.org/node/1897420 for further
information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Configuration > User Interface
      > Breadcrumb Field settings
    for configurations. Save Configurations.
       (<your_site_url>/admin/config/user-interface/breadcrumb_field_form)
    3. Add the breadcrumb field to the node or taxonomy term entities.
    4. Notice the new breadcrumb field wrapper in the edit form.
    5. Add the links you need.
    6. Require to re-save display on
       admin/structure/types/manage/{your_node_type}/display (known bug)

Configurable parameters:
* Include / Exclude the front page as a segment in the breadcrumb.
* Include / Exclude the current page as the last segment in the breadcrumb.
* Use the real page title when it is available instead of always deducing it
  from the URL.
* Print the page's title segment as a link.
* Use a custom separator between the breadcrumb's segments. (TODO)
* Choose a transformation mode for the segments' title. (TODO)
* Make the 'capitalizator' ignore some words. (TODO)

-----------------------


MAINTAINERS
-----------

* Yurii Stenin (yurii.stenin@dropsolid.com)

Supporting organization:

* Dropsolid - https://www.drupal.org/dropsolid for customer Teamleader
