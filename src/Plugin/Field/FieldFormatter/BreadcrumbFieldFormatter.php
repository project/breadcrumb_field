<?php

namespace Drupal\breadcrumb_field\Plugin\Field\FieldFormatter;

use Drupal\breadcrumb_field\BreadcrumbFieldBuilder;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Path\PathValidatorInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\link\Plugin\Field\FieldFormatter\LinkFormatter;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'breadcrumb_field_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "breadcrumb_field_formatter",
 *   label = @Translation("Breadcrumb field formatter"),
 *   field_types = {
 *     "breadcrumb_field_type"
 *   }
 * )
 */
class BreadcrumbFieldFormatter extends LinkFormatter implements ContainerFactoryPluginInterface {

  /**
   * BreadcrumbFieldBuilder definition.
   *
   * @var \Drupal\breadcrumb_field\BreadcrumbFieldBuilder
   */
  private BreadcrumbFieldBuilder $breadcrumbFieldBuilder;

  /**
   * Constructs a new BreadcrumbFieldFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Third party settings.
   * @param \Drupal\Core\Path\PathValidatorInterface $path_validator
   *   The path validator service.
   * @param \Drupal\breadcrumb_field\BreadcrumbFieldBuilder $breadcrumb_field_builder
   *   The breadcrumb builder.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, PathValidatorInterface $path_validator, BreadcrumbFieldBuilder $breadcrumb_field_builder) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings, $path_validator);
    $this->breadcrumbFieldBuilder = $breadcrumb_field_builder;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('path.validator'),
      $container->get('breadcrumb_field.breadcrumb')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $element = [];
    // Send items to Breadcrumb builder.
    $this->breadcrumbFieldBuilder->setFieldLinks($items);
    return $element;
  }

}
