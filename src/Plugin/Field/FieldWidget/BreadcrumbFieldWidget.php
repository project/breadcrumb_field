<?php

namespace Drupal\breadcrumb_field\Plugin\Field\FieldWidget;

use Drupal\link\Plugin\Field\FieldWidget\LinkWidget;

/**
 * Plugin implementation of the 'breadcrumb_field_widget' widget.
 *
 * @FieldWidget(
 *   id = "breadcrumb_field_widget",
 *   label = @Translation("Breadcrumb field widget"),
 *   field_types = {
 *     "breadcrumb_field_type"
 *   }
 * )
 */
class BreadcrumbFieldWidget extends LinkWidget {}
