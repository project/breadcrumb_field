<?php

namespace Drupal\breadcrumb_field;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Component\Utility\Unicode;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Access\AccessManagerInterface;
use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Controller\TitleResolverInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Menu\MenuLinkManager;
use Drupal\Core\ParamConverter\ParamNotConvertedException;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\PathProcessor\InboundPathProcessorInterface;
use Drupal\Core\Routing\RequestContext;
use Drupal\Core\Routing\RouteMatch;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\Core\Utility\Error;
use Drupal\node\NodeInterface;
use Drupal\taxonomy\TermInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Matcher\RequestMatcherInterface;

/**
 * Class Breadcrumb Field Builder.
 *
 * @package Drupal\breadcrumb_field
 */
class BreadcrumbFieldBuilder implements BreadcrumbBuilderInterface {
  use StringTranslationTrait;

  /**
   * The router request context.
   *
   * @var \Drupal\Core\Routing\RequestContext
   */
  protected RequestContext $context;

  /**
   * The menu link access service.
   *
   * @var \Drupal\Core\Access\AccessManagerInterface
   */
  protected AccessManagerInterface $accessManager;

  /**
   * The dynamic router service.
   *
   * @var \Symfony\Component\Routing\Matcher\RequestMatcherInterface
   */
  protected RequestMatcherInterface $router;

  /**
   * The dynamic router service.
   *
   * @var \Drupal\Core\PathProcessor\InboundPathProcessorInterface
   */
  protected InboundPathProcessorInterface $pathProcessor;

  /**
   * Breadcrumb config object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * Site config object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $siteConfig;

  /**
   * The title resolver.
   *
   * @var \Drupal\Core\Controller\TitleResolverInterface
   */
  protected TitleResolverInterface $titleResolver;

  /**
   * The current user object.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected AccountInterface $currentUser;

  /**
   * FieldItemListInterface definition.
   *
   * @var \Drupal\Core\Field\FieldItemListInterface
   */
  protected FieldItemListInterface $fieldLinks;

  /**
   * The menu link manager.
   *
   * @var \Drupal\Core\Menu\MenuLinkManager
   */
  protected MenuLinkManager $menuLinkManager;

  /**
   * The CurrentPathStack definition.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected CurrentPathStack $currentPath;

  /**
   * RouteMatchInterface definition.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected RouteMatchInterface $routeMatch;

  /**
   * LanguageManagerInterface definition.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * LoggerInterface definition.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * Constructs the PathBasedBreadcrumbBuilder.
   *
   * @param \Drupal\Core\Routing\RequestContext $context
   *   The router request context.
   * @param \Drupal\Core\Access\AccessManagerInterface $access_manager
   *   The menu link access service.
   * @param \Symfony\Component\Routing\Matcher\RequestMatcherInterface $router
   *   The dynamic router service.
   * @param \Drupal\Core\PathProcessor\InboundPathProcessorInterface $path_processor
   *   The inbound path processor.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Controller\TitleResolverInterface $title_resolver
   *   The title resolver service.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user object.
   * @param \Drupal\Core\Path\CurrentPathStack $current_path
   *   The current path.
   * @param \Drupal\Core\Menu\MenuLinkManager $menu_link_manager
   *   The menu link manager.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   */
  public function __construct(
    RequestContext $context,
    AccessManagerInterface $access_manager,
    RequestMatcherInterface $router,
    InboundPathProcessorInterface $path_processor,
    ConfigFactoryInterface $config_factory,
    TitleResolverInterface $title_resolver,
    AccountInterface $current_user,
    CurrentPathStack $current_path,
    MenuLinkManager $menu_link_manager,
    RouteMatchInterface $route_match,
    LanguageManagerInterface $language_manager,
    LoggerInterface $logger,
  ) {
    $this->context = $context;
    $this->accessManager = $access_manager;
    $this->router = $router;
    $this->pathProcessor = $path_processor;
    $this->config = $config_factory->get('breadcrumb_field.settings');
    $this->siteConfig = $config_factory->get('system.site');
    $this->titleResolver = $title_resolver;
    $this->currentUser = $current_user;
    $this->currentPath = $current_path;
    $this->menuLinkManager = $menu_link_manager;
    $this->routeMatch = $route_match;
    $this->languageManager = $language_manager;
    $this->logger = $logger;
  }

  /**
   * Whether this breadcrumb builder should be used to build the breadcrumb.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   *
   * @return bool
   *   TRUE if this builder should be used or FALSE to let other builders
   *   decide.
   */
  public function applies(RouteMatchInterface $route_match): bool {
    $parameters = $route_match->getParameters();
    foreach ($parameters as $parameter) {
      // Add support for nodes and Terms only.
      if (($parameter instanceof NodeInterface || $parameter instanceof TermInterface) && isset($this->fieldLinks) && !$this->fieldLinks->isEmpty()) {
        $entity = $this->fieldLinks->getEntity();
        // It applies only on page with values from breadcrumb field.
        return $entity->id() === $parameter->id();
      }
    }
    return FALSE;
  }

  /**
   * Provides node from route.
   *
   * @return bool|\Drupal\node\NodeInterface
   *   The node object.
   */
  public function getCurrentNode(): NodeInterface|bool {
    $node = $this->routeMatch->getParameter('node');
    return $node instanceof NodeInterface ? $node : FALSE;
  }

  /**
   * Provides the term from route.
   *
   * @return \Drupal\taxonomy\TermInterface|bool
   *   The term object.
   */
  public function getCurrentTerm(): TermInterface|bool {
    $term = $this->routeMatch->getParameter('taxonomy_term');
    return $term instanceof TermInterface ? $term : FALSE;
  }

  /**
   * Creates the link.
   *
   * @param \Drupal\Core\Url $url
   *   The url object.
   * @param string $title
   *   The title.
   *
   * @return \Drupal\Core\Link
   *   The link.
   */
  protected function createLinkFromUrl(Url $url, string $title): Link {
    if ($url->isRouted()) {
      $params = $url->getRouteParameters();
      if (\count($params) > 0) {
        $entity_type = key($params);
        $node_id = current($params);
        return Link::createFromRoute($title, 'entity.node.canonical', [$entity_type => $node_id]);
      }
    }
    return Link::fromTextAndUrl($title, $url);
  }

  /**
   * Builds the breadcrumb.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   *
   * @return \Drupal\Core\Breadcrumb\Breadcrumb
   *   A breadcrumb.
   */
  public function build(RouteMatchInterface $route_match): Breadcrumb {
    $breadcrumb = new Breadcrumb();
    $breadcrumb->addCacheContexts(['url.path', 'languages', 'url.query_args']);
    $breadcrumb->addCacheableDependency($this->config);

    $links = [];

    if ($this->config->get(BreadcrumbFieldConstants::INCLUDE_HOME_SEGMENT->value)) {
      $links = $this->addHomeLink($links);
    }

    if (isset($this->fieldLinks) && !$this->fieldLinks->isEmpty()) {
      $links = $this->addBreadcrumbFieldLinks($links, $breadcrumb);
    }

    if ($this->config->get(BreadcrumbFieldConstants::INCLUDE_TITLE_SEGMENT->value)) {
      $links = $this->addTitleSegment($links, $breadcrumb);
    }

    if ($this->config->get(BreadcrumbFieldConstants::HIDE_SINGLE_HOME_ITEM->value) && count($links) === 1) {
      return $breadcrumb->setLinks([]);
    }

    return $breadcrumb->setLinks($links);
  }

  /**
   * Adds home link to the main links.
   *
   * @param array $links
   *   The links.
   *
   * @return array
   *   The links for breadcrumb.
   */
  protected function addHomeLink(array $links): array {
    $path = trim($this->context->getPathInfo(), '/');
    $path = urldecode($path);
    $front = $this->siteConfig->get('page.front');
    $curr_lang = $this->languageManager->getCurrentLanguage()->getId();

    if ($path && '/' . $path != $front && $path != $curr_lang) {
      $links[] = Link::createFromRoute($this->config->get(BreadcrumbFieldConstants::HOME_SEGMENT_TITLE->value), '<front>');
    }

    return $links;
  }

  /**
   * Adds the links from breadcrumb field.
   *
   * @param array $links
   *   The main links list.
   * @param \Drupal\Core\Breadcrumb\Breadcrumb $breadcrumb
   *   The breadcrumb object.
   *
   * @return array
   *   The links list.
   */
  protected function addBreadcrumbFieldLinks(array $links, Breadcrumb $breadcrumb): array {
    foreach ($this->fieldLinks->getValue() as $field_value) {
      try {
        $url = Url::fromUri($field_value['uri']);
        $links[] = $this->createLinkFromUrl($url, $field_value['title']);
      }
      catch (\UnexpectedValueException $exception) {
        Error::logException($this->logger, $exception);
        break;
      }
    }

    $node = $this->getCurrentNode();
    if ($node) {
      $breadcrumb->addCacheableDependency($node);
    }

    $term = $this->getCurrentTerm();
    if ($term) {
      $breadcrumb->addCacheableDependency($term);
    }

    return $links;
  }

  /**
   * Provides the title segment.
   *
   * @param array $links
   *   The main links list.
   * @param \Drupal\Core\Breadcrumb\Breadcrumb $breadcrumb
   *   The breadcrumb object.
   *
   * @return array
   *   The links list.
   */
  protected function addTitleSegment(array $links, Breadcrumb $breadcrumb): array {
    $path = trim($this->context->getPathInfo(), '/');
    $path = urldecode($path);
    $path_elements = explode('/', $path);
    $path_elements = [array_pop($path_elements)];
    $check_path = '/' . implode('/', $path_elements);
    $exclude = [];

    $route_request = $this->getRequestForPath($check_path, $exclude);
    if ($route_request) {
      $route_match = RouteMatch::createFromRequest($route_request);
      $access = $this->accessManager->check($route_match, $this->currentUser, NULL, TRUE);
      $breadcrumb->addCacheableDependency($access);

      if ($access->isAllowed()) {
        $title = $this->getTitleString($route_request, $route_match);

        if (!isset($title)) {
          $route_name = $route_match->getRouteName();
          $route_parameters = $route_match->getRawParameters()->all();
          $menu_links = $this->menuLinkManager->loadLinksByRoute($route_name, $route_parameters);

          if (!empty($menu_links)) {
            $menu_link = reset($menu_links);
            $title = $menu_link->getTitle();
          }
        }

        if (!isset($title)) {
          $title = str_replace(['-', '_'], ' ', Unicode::ucfirst(end($path_elements)));
        }

        if (!$this->config->get(BreadcrumbFieldConstants::TITLE_SEGMENT_AS_LINK->value)) {
          $links[] = Link::createFromRoute($title, '<none>');
        }
        else {
          $url = Url::fromRouteMatch($route_match);
          $links[] = new Link($title, $url);
        }
      }
    }
    elseif (empty($exclude[implode('/', $path_elements)])) {
      $title = str_replace(['-', '_'], ' ', Unicode::ucfirst(end($path_elements)));
      $links[] = Link::createFromRoute($title, '<none>');
    }

    return $links;
  }

  /**
   * Setter for fields items.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $fieldItemList
   *   The field list.
   */
  public function setFieldLinks(FieldItemListInterface $fieldItemList): static {
    $this->fieldLinks = $fieldItemList;
    return $this;
  }

  /**
   * Get string title for route.
   *
   * @param \Symfony\Component\HttpFoundation\Request $route_request
   *   A request object.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   *
   * @return string|false
   *   Either the current title string or FALSE if unable to determine it.
   */
  public function getTitleString(Request $route_request, RouteMatchInterface $route_match): false|string {
    $title = $this->titleResolver->getTitle($route_request, $route_match->getRouteObject());
    // Get string from title. Different routes return different objects.
    // Many routes return a translatable markup object.
    if ($title instanceof TranslatableMarkup) {
      $title = $title->render();
    }
    elseif ($title instanceof FormattableMarkup) {
      $title = (string) $title;
    }

    // Other paths, such as admin/structure/menu/manage/main, will
    // return a render array suitable to render using core's XSS filter.
    elseif (is_array($title) && array_key_exists('#markup', $title)) {
      // If this render array has #allowed tags use that instead of default.
      $tags = array_key_exists('#allowed_tags', $title) ? $title['#allowed_tags'] : NULL;
      $title = Xss::filter($title['#markup'], $tags);
    }

    // If a route declares the title in an unexpected way log and return FALSE.
    if (!is_string($title)) {
      $this->logger
        ->notice($this->t('Breadcrumb field could not determine the title to use for @path', [
          '@path' => $route_match->getRouteObject()
            ->getPath(),
        ]));
      return FALSE;
    }
    return $title;
  }

  /**
   * Matches a path in the router.
   *
   * @param string $path
   *   The request path with a leading slash.
   * @param array $exclude
   *   An array of paths or system paths to skip.
   *
   * @return \Symfony\Component\HttpFoundation\Request|null
   *   A populated request object or NULL if the path couldn't be matched.
   */
  protected function getRequestForPath($path, array $exclude): ?Request {
    if (!empty($exclude[$path])) {
      return NULL;
    }
    $request = Request::create($path);
    // Performance optimization: set a short accept header to reduce overhead in
    // AcceptHeaderMatcher when matching the request.
    $request->headers->set('Accept', 'text/html');
    // Find the system path by resolving aliases, language prefix, etc.
    $processed = $this->pathProcessor->processInbound($path, $request);
    if (empty($processed) || !empty($exclude[$processed])) {
      // This resolves to the front page, which we already add.
      return NULL;
    }
    $this->currentPath->setPath($processed, $request);
    // Attempt to match this path to provide a fully built request.
    try {
      $request->attributes->add($this->router->matchRequest($request));
      return $request;
    }
    catch (ParamNotConvertedException | ResourceNotFoundException | MethodNotAllowedException | AccessDeniedHttpException $e) {
      return NULL;
    }
  }

}
